package com.example.mymap;

import androidx.fragment.app.FragmentActivity;

import android.app.Dialog;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.mymap.databinding.ActivityMapsBinding;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ActivityMapsBinding binding;
    private Marker marker;

    private EditText etSearch;
    private Button btnSearch;

    private static final int CAMERA_ZOOM_VALUE = 7;
    private static final int MAX_ADDRESSES_RESULTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        etSearch = (EditText) findViewById(R.id.etSearch);
        btnSearch = (Button) findViewById(R.id.btnSearch);

        //if the search button is hit, run geocode function
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == btnSearch)
                {
                    geocode();
                }
            }
        });

        if (isGoogleApiAvailabe())
        {
            initMap();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }


    //this function initials the map and runs onMapReady function.
    private void initMap()
    {
        if(mMap == null)
        {
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }


    //this function checks rather google API is available or not on the device.
    //if the issue is resolvable, it starts a dialog.
    public boolean isGoogleApiAvailabe() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        }

        else if (api.isUserResolvableError(isAvailable)) {
            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        }

        else {
            Toast.makeText(this, "Can't use google API", Toast.LENGTH_LONG).show();
        }

        return false;
    }

    //this function moves the camera to new location with specific zoom
    public void setNewLocation(LatLng latlng, int zoom)
    {
        CameraUpdate c = CameraUpdateFactory.newLatLngZoom(latlng,zoom);
        mMap.moveCamera(c);
    }


    //this function sets a marker to a specific location with a title.
    public void setMarker(LatLng latlng, Address address)
    {
        if (marker != null)
        {
            marker.remove();
        }
        MarkerOptions markerOptions = new MarkerOptions().title(address.getLocality()).position(latlng);
        marker = mMap.addMarker(markerOptions);
    }


    //this function converts text address to latitude and longitude,
    //moves the camera and sets there marker
    public void geocode()
    {
        Geocoder geocoder = new Geocoder(this);

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocationName(etSearch.getText().toString(),MAX_ADDRESSES_RESULTS);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses!=null)
        {
            Address address = addresses.get(0);
            LatLng latlng = new LatLng(address.getLatitude(), address.getLongitude());
            setNewLocation(latlng,CAMERA_ZOOM_VALUE);
            setMarker(latlng, address);
        }
    }
}